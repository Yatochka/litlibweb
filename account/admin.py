from django.contrib import admin
from .models import UserProfile


class ProfileAdmin(admin.ModelAdmin):

    fields = ('user','sex','birth_date','img_url','bio','followers', 'avatar')
    list_display = ('user','sex','birth_date','bio', 'avatar')
    list_filter = ('user',)


admin.site.register(UserProfile, ProfileAdmin)
