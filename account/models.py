from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.conf import settings
from django.urls import reverse


class UserProfile(models.Model):

    SEX_CHOISES = (
        ('Battle helicopter', 'Battle helicopter'),
        ('Male', 'Male'),
        ('Female', 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    sex = models.CharField(max_length=20, default='Male', choices=SEX_CHOISES)
    bio = models.TextField(max_length=500, blank=True,)
    birth_date = models.DateField(null=True, blank=True)
    img_url = models.URLField(default='')
    followers = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='followers')
    avatar = models.FileField( blank=True, default='profile_photos/colorful-octopus.jpg', upload_to='profile_photos/', )

    def get_absolute_url(self):
        return reverse("blog:uPage", kwargs={"username": self.user.username})