from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from account.forms import SignUpForm, ProfileForm
from django.core.files.storage import FileSystemStorage

def signup(request):
    if request.method == 'POST' :
        form = SignUpForm(request.POST)
        profile_form = ProfileForm(request.POST, request.FILES)
        if form.is_valid() and profile_form.is_valid():
            form.save()
            profile = profile_form.save(commit=False)
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            profile.user = user
            profile.birth_date = profile_form.cleaned_data.get('birth_date')
            #profile.avatar = profile_form.cleaned_data.get('file')

            profile.save()
            login(request, user)
            return redirect('blog:blog_list_view')
    else:
        form = SignUpForm()
        profile_form = ProfileForm()
    return render(request, 'registration/register.html', {'form': form,
                                                          'profile': profile_form })


'''@login_required
@transaction.atomic
def update_profile(request):
    if request.method == 'POST':
        user_form = SignUpForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, _('Your profile was successfully updated!'))
            return redirect('settings:profile')
        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        user_form = SignUpForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'profiles/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })'''