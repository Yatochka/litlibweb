# Generated by Django 2.1.2 on 2018-12-05 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='bio',
            field=models.TextField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='sex',
            field=models.CharField(choices=[('Battle helicopter', 'Battle helicopter'), ('Male', 'Male'), ('Female', 'Female')], default='Male', max_length=20),
        ),
    ]
