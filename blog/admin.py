from django.contrib import admin
from .models import Post, Category, Comment


class PostAdmin(admin.ModelAdmin):
    list_display = ('title','category', 'author', 'updated', 'created', 'status',)
    list_filter = ('status', 'created',)
    search_fields = ('title', 'body',)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_filter = ('title', )
    search_fields = ('title',)

class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', )
    list_filter = ('author',)
    search_fields = ('author',)

admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment, CommentAdmin)