from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.conf import settings

class Category(models.Model):
    title = models.CharField(max_length=100 )
    seo_title = models.CharField(max_length=100, blank=True, null=True)
    slug = models.SlugField(max_length=250, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Post(models.Model):

    STATUS_CHOISES = (
        ('Published', 'Published'),
        ('Draft', 'Draft'),
    )

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default="1")
    title = models.CharField(max_length=30)
    body = models.TextField()
    category = models.ForeignKey(Category,on_delete=models.CASCADE, default=1)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    likes= models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='post_likes')
    slug = models.SlugField(max_length=250, unique=True)
    status = models.CharField(max_length=10, default='Draft', choices=STATUS_CHOISES)
    image = models.FileField( blank=True, default='post_photos/colorful-octopus.jpg', upload_to='post_photos/', )

    def get_absolute_url(self):
        return reverse("blog:blog_detail", kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_like_url(self):
        return reverse("blog:like-toggle", kwargs={"slug": self.slug})

    def get_api_like_url(self):
        return reverse("blog:like-api-toggle", kwargs={"slug": self.slug})


class Comment(models.Model):

    post = models.ForeignKey('blog.Post', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

    def destruction(self, time):
        if time == self.created_date:
            self.delete()