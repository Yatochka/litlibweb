# Generated by Django 2.1.2 on 2018-11-07 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SiteUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=30, unique=True)),
                ('password', models.CharField(max_length=30)),
                ('bio', models.CharField(max_length=500)),
                ('email', models.CharField(max_length=30, unique=True)),
            ],
        ),
    ]
