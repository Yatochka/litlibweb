from django.conf.urls import url
from . import views as core_view
app_name = 'blog'

urlpatterns = [
   url(r'^$', core_view.post_list,  name="blog_list_view"),
   url(r'post/(?P<slug>[-\w]+)/', core_view.post_detail, name="blog_detail"),
   url(r'(?P<slug>[-\w]+)/like/', core_view.PostLikeToggle.as_view(), name="like-toggle"),
   url(r'new-post/', core_view.post_new, name="new_post"),
   url(r'edit/(?P<slug>[-\w]+)/', core_view.post_edit, name='edit_post'),
   url(r'(?P<slug>[-\w]+)/delete/',core_view.delete_post,name='delete_post'),
   url(r'result/', core_view.search, name="search"),
   url(r'category/(?P<slug>[-\w]+)/', core_view.category_detail, name='category_detail'),
   url(r'(?P<slug>[-\w]+)/comment/', core_view.add_comment_to_post, name='add_comment'),
  # url(r'comment/(?P<slug>\d+)/delete/', core_view.DelComment.as_view(), name='delete_coment'),
   #url(r'(?P<slug>\d+)/comment/delete/', core_view.delete_comment, name='delete_comment'),
   url(r'profile/(?P<username>[-\w]+)/', core_view.get_user_profile, name='uPage'),
   url(r'follow/(?P<username>[-\w]+)/', core_view.FollowToggle.as_view(), name="follow-toggle"),
   url(r'delete/(?P<username>[-\w]+)/',core_view.delete_user,name='delete_user'),
]