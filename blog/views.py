from django.db.models import Q
from django.shortcuts import render, get_object_or_404,redirect
from .models import Post, Category, Comment
from .forms import PostForm, CommentForm
from django.utils import timezone
from paginator.config import pagination
from django.views.generic import RedirectView
from account.models import UserProfile
from django.contrib.auth.models import User
from django.urls import reverse


def post_list(request):

    template = 'pages/post_list.html'
    items = Post.objects.order_by('-created')
    items = pagination(request, items, 5)
    category = Category.objects.all()
    context = {

       'items': items[0],
       'page_range': items[1],
       'category': category,
    }

    return render(request, template, context)

class PostLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        slug = self.kwargs.get("slug")
        print(slug)
        obj = get_object_or_404(Post, slug=slug)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user)
        return url_

def post_detail(request, slug):

    template = 'pages/post_detail.html'
    post = get_object_or_404(Post, slug=slug)
    category = Category.objects.all()
    context = {
        'post': post,
        'category': category,
    }
    return render(request, template, context)


def post_new(request):

    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.created = timezone.now()
            post.save()
            return redirect('blog:blog_list_view' )
    else:
        form = PostForm()
    return render(request, 'pages/post_edit.html', {'form': form})


def post_edit(request, slug):
    template = 'pages/post_edit.html'
    post = get_object_or_404(Post, slug=slug)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            return redirect('blog:blog_detail', slug=post.slug)
    else:
        form = PostForm(instance=post)
    return render(request, 'pages/post_edit.html', {'form': form})


def delete_post(request, slug):
    post = get_object_or_404(Post, slug=slug)
    post.delete()
    return redirect('blog:blog_list_view' )


def category_list(request):

    template = 'pages/post_list.html'
    categories = Category.objects.all()
    content = {
        'categories': categories,
    }
    return render(request, template, content)


def category_detail(request, slug):
    categoryC = get_object_or_404(Category, slug=slug)
    template = "pages/post_list.html"
    items = Post.objects.filter(category=categoryC).order_by("-created")
    items = pagination(request, items, 10)
    category = Category.objects.all()
    context = {

        'items': items[0],
        'page_range': items[1],
        'category': category,
    }

    return render(request, template, context)


def search(request):

    template = "pages/post_list.html"
    query = request.GET.get('q')
    if query:
        result = Post.objects.filter(Q(title__icontains=query) | Q(body__icontains=query))
    else:
        result = Post.objects.filter(status="Published")
    items = pagination(request, result)
    category = Category.objects.all()
    content = {
        'items': items[0],
        'page_range': items[1],
        'category': category,
    }

    return render(request, template, content)


def add_comment_to_post(request, slug):

    post = get_object_or_404(Post, slug=slug)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('blog:blog_detail', slug=post.slug)
    else:
        form = CommentForm()
    return render(request, 'pages/add_comment_to_post.html', {'form': form, 'post': post})


class DelComment(RedirectView):
    def del_comment(request, time):

        comment = get_object_or_404(Comment, time)
        post = comment.post
        comment.delete()
        return reverse("blog:blog_detail", post.slug)

def get_user_profile(request, username):

    user = User.objects.get(username=username)
    articles = Post.objects.filter(author=user)
    category = Category.objects.all()
    profile = UserProfile.objects.get(user=user)
    context = {
        'user': user,
        'articles': articles,
        'category': category,
        'profile': profile,
    }
    return render(request, 'pages/user_page.html', context,)

class FollowToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        username = self.kwargs.get("username")
        us = get_object_or_404(User, username=username)
        obj = get_object_or_404(UserProfile, user=us)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.followers.all():
                obj.followers.remove(user)
            else:
                obj.followers.add(user)
        return url_

def delete_user(request, username):

    user = get_object_or_404(User, username=username)
    profile = get_object_or_404(UserProfile,user=user)
    profile.delete()
    user.delete()
    return redirect('blog:blog_list_view')